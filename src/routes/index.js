var express = require('express');
const config = require('../config/index')

var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/test', function(req, res, next) {
  res.json({data: config.host})
});

module.exports = router;
